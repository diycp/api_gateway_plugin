转移请求插件
======

**1. 功能说明**

把请求转到已配置的host和path

**2. 插件配置说明**

在[微信API网关](https://gitee.com/libfintech/wechat_api_gateway)的[/plugin_config/transfer](https://gitee.com/libfintech/wechat_api_gateway/blob/master/plugin_config/transfer)目录中，复制[xxx.conf.js.example](https://gitee.com/libfintech/wechat_api_gateway/blob/master/plugin_config/transfer/xxx.conf.js.example)为xxx.conf.js，其中xxx可自定义，具体配置内容参考[xxx.conf.js.example](https://gitee.com/libfintech/wechat_api_gateway/blob/master/plugin_config/transfer/xxx.conf.js.example)文件。