"use strict";
/**
 * 转移请求
 * Created by liamjung on 2018/6/12.
 */
const {ResponseError} = require("@libfintech/api-gateway-core/error/response_error");
const {matchConfig} = require("@libfintech/api-gateway-core//util/util");

async function main(pipeline, configs) {

    let req = pipeline.request;

    let config = matchConfig(req, configs);

    if (!config) {
        //配置不存在时，忽略
        return;
    }

    req.host = config.host;

    if (config.pathMap)
        req.path = config.pathMap(req.path);
}

module.exports = {
    main: main
};