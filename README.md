[API网关](https://gitee.com/libfintech/wechat_api_gateway)插件库
========

## 插件列表
- [IP白名单/黑名单](https://gitee.com/libfintech/api_gateway_plugin/tree/master/ip)（ip）
- [日志追踪](https://gitee.com/libfintech/api_gateway_plugin/tree/master/log_trace)（log_trace）
- [反向代理](https://gitee.com/libfintech/api_gateway_plugin/tree/master/proxy)（proxy）
- [转移请求](https://gitee.com/libfintech/api_gateway_plugin/tree/master/transfer)（transfer）
- [微信网关](https://gitee.com/libfintech/api_gateway_plugin/tree/master/wx_gateway)（wx_gateway & [wx_gateway_response](https://gitee.com/libfintech/api_gateway_plugin/tree/master/wx_gateway_response)）

## Installation

```sh
$ npm install @libfintech/api-gateway-plugin --save
```

## 自定义插件开发
- 插件入口：index.js
- 插件配置例子：xxx.conf.js.example，xxx为配置名称，可自定义配置名称，配置文件可有多个，需保存到应用的[插件配置目录](https://gitee.com/libfintech/wechat_api_gateway/tree/master/plugin_config)中，并在[插件配置](https://gitee.com/libfintech/wechat_api_gateway/blob/master/app_config.js.example)中指定配置目录

具体参考[插件模板](https://gitee.com/libfintech/api_gateway_plugin/tree/master/_____temp)