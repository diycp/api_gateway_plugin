"use strict";
/**
 * 反向代理
 * Created by liamjung on 2018/6/12.
 */
const {executeRest} = require("@libfintech/api-gateway-core/util/rest_util");
const {ResponseError} = require("@libfintech/api-gateway-core/error/response_error");

async function main(pipeline, configs) {

    let req = pipeline.request;

    if (!req.url) {
        //url未被赋值时，抛出异常
        throw new ResponseError(404);
    }

    let [resp, err] = await executeRest(req, pipeline.logger);

    if (resp) {
        //响应客户端
        pipeline.response.update(resp);
    } else {
        pipeline.logger.error(JSON.stringify(err));

        //404
        throw new ResponseError(404);
    }
}

module.exports = {
    main: main
};