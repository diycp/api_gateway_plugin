"use strict";
/**
 * 响应从微信接收消息
 * Created by liamjung on 2018/6/28.
 */
const {getResp} = require("../util/wx_response_util");

module.exports = async function (pipeline) {

    let content = pipeline.response.body;
    let wxMsg = pipeline.ctx.weixin_msg;
    let encryptType = pipeline.request.query.encrypt_type;

    pipeline.logger.debug("wx request encrypt type: " + encryptType);
    pipeline.logger.debug("wx response content: " + content);

    if (!content || content === "null") {
        //假如服务器无法保证在五秒内处理并回复，可以直接回复空串，微信服务器不会对此作任何处理，并且不会发起重试
        //https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140453
        content = "";
    }

    let resp = getResp(content, wxMsg, encryptType);

    pipeline.response.update(resp);
};