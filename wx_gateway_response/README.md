微信网关响应插件
========

**1. 功能说明**

处理微信请求或发到微信的请求通过反向代理后生成的响应（必须与[微信网关](https://gitee.com/libfintech/api_gateway_plugin/blob/master/wx_gateway)插件配对使用）

**2. 插件配置说明**

参考[微信网关](https://gitee.com/libfintech/api_gateway_plugin/blob/master/wx_gateway)的插件配置说明
