"use strict";
/**
 * 微信网关响应
 * Created by liamjung on 2018/6/27.
 */
const {matchConfig} = require("@libfintech/api-gateway-core/util/util");

async function main(pipeline, configs) {

    let req = pipeline.request;

    let config = matchConfig(req, configs);

    if (!config) {
        //配置不存在时，忽略
        return;
    }

    await config.controller(pipeline);
}

module.exports = {
    main: main
};