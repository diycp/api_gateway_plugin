IP白名单/黑名单插件
===========

**1. 功能说明**

过滤和筛选客户端IP地址

**2. 插件配置说明**

在[微信API网关](https://gitee.com/libfintech/wechat_api_gateway)的[/plugin_config/ip](https://gitee.com/libfintech/wechat_api_gateway/blob/master/plugin_config/ip)目录中，复制[xxx.conf.js.example](https://gitee.com/libfintech/wechat_api_gateway/blob/master/plugin_config/ip/xxx.conf.js.example)为xxx.conf.js，其中xxx可自定义，具体配置内容参考[xxx.conf.js.example](https://gitee.com/libfintech/wechat_api_gateway/blob/master/plugin_config/ip/xxx.conf.js.example)文件。