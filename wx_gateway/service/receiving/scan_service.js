"use strict";
/**
 * 扫码事件服务
 * Created by liamjung on 2018/6/25.
 */
const AbstractService = require("../abstract_service");

class ScanService extends AbstractService {

    async execute(wxMsg, mpConfig, pipeline) {

        let openId = wxMsg.FromUserName;

        //TODO 集成调试机制
        let accountSessionId;// = DebugUtil.getAccountSessionId(wxMessage.getEventKey());

        if (accountSessionId) {
            //关注后，扫码调试时，推送微信消息

            // DebugUtil.sendDebugMsg(this.mpService, accountSessionId, config, openId, this.debugAuthenticationUrl, DebugMsgTypeEnum.START_DEBUG);
        } else {
            let config = super.getRouteConfig(mpConfig, "scan");

            let createTime = +wxMsg.CreateTime;

            //ScanDTO
            let dto = {
                subjectId: mpConfig.subjectId,
                openId: openId,
                mpId: mpConfig.mpId,
                createTime: createTime,

                sceneId: +wxMsg.EventKey
            };

            super.updateRequest(pipeline.request, config, dto);
        }
    }
}

module.exports = new ScanService();