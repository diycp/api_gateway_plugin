"use strict";
/**
 * 点击菜单（非链接）事件服务
 * Created by liamjung on 2018/6/25.
 */
const {ResponseWxError} = require("../../error/response_wx_error");
const AbstractService = require("../abstract_service");

class MenuService extends AbstractService {

    async execute(wxMsg, mpConfig, pipeline) {

        let menuKey;
        try {
            menuKey = JSON.parse(wxMsg.EventKey);

            //测试数据
            // menuKey = {
            //     type: "text",
            //     content: "test_content"
            // }
        } catch (err) {

            let respWxError = new ResponseWxError("event key (" + wxMsg.EventKey + ") parse error.");

            pipeline.logger.warn(respWxError.message);

            throw respWxError;
        }

        if (menuKey.type === "text" || menuKey.type === "image") {

            let openId = wxMsg.FromUserName;
            let createTime = +wxMsg.CreateTime;

            let config = super.getRouteConfig(mpConfig, "menu");

            //MenuDTO
            let dto = {
                subjectId: mpConfig.subjectId,
                openId: openId,
                mpId: mpConfig.mpId,
                createTime: createTime,

                content: menuKey.content
            };

            super.updateRequest(pipeline.request, config, dto);
        }
    }
}

module.exports = new MenuService();