"use strict";
/**
 * 取消关注事件服务
 * Created by liamjung on 2018/6/25.
 */
const AbstractService = require("../abstract_service");

class UnsubscribeService extends AbstractService {

    async execute(wxMsg, mpConfig, pipeline) {

        let openId = wxMsg.FromUserName;
        let createTime = +wxMsg.CreateTime;

        let config = super.getRouteConfig(mpConfig, "unsubscribe");

        //UnsubscribeDTO
        let dto = {
            subjectId: mpConfig.subjectId,
            openId: openId,
            mpId: mpConfig.mpId,
            createTime: createTime

            //TODO 待完善
        };

        super.updateRequest(pipeline.request, config, dto);
    }
}

module.exports = new UnsubscribeService();