"use strict";
/**
 * 抽象控制器
 * Created by liamjung on 2018/6/25.
 */
const {ResponseWxError} = require("../error/response_wx_error");
const {ResponseError} = require("@libfintech/api-gateway-core/error/response_error");

module.exports = class AbstractService {

    /**
     * 执行
     * 需在子类中实现
     * @param data      数据
     * @param mpConfig  公众号配置
     * @param pipeline  管道
     */
    async execute(data, mpConfig, pipeline) {
    }

    /**
     * 获取微信模板id
     * @param mpConfig  公众号配置
     * @param key       业务标识
     */
    getTemplateId(mpConfig, key) {

        let templates = mpConfig["templates"];

        if (!templates) {
            //配置不存在时，抛出异常

            Log.warn("There is no templates config");

            throw new ResponseError(404);
        }

        let templateId = templates[key];

        if (!templateId) {
            //配置不存在时，抛出异常

            Log.warn("There is no template config named \"" + key + "\"");

            throw new ResponseError(404, undefined, "公众号配置的指定微信模板不存在");
        }

        return templateId;
    }

    /**
     * 获取普通模板路径
     * @param mpConfig  公众号配置
     * @param key       业务标识
     */
    getMsgTempPath(mpConfig, key) {

        let msgTemps = mpConfig["msgTemps"];

        if (!msgTemps) {
            //配置不存在时，抛出异常

            Log.warn("There is no msg temps config");

            throw new ResponseError(404);
        }

        let msgTempPath = msgTemps[key];

        if (!msgTempPath) {
            //配置不存在时，抛出异常

            Log.warn("There is no msg temp config named \"" + key + "\"");

            throw new ResponseError(404, undefined, "公众号配置的指定普通模板不存在");
        }

        return msgTempPath;
    }

    /**
     * 获取route配置
     * @param mpConfig  公众号配置
     * @param key       业务标识
     * @returns {{host, uri}}
     */
    getRouteConfig(mpConfig, key) {

        let routes = mpConfig["routes"];

        if (!routes) {
            //配置不存在时，直接响应微信
            throw new ResponseWxError("");
        }

        let config = routes[key];

        if (!config) {
            //配置不存在时，直接响应微信
            throw new ResponseWxError("");
        }

        //防止篡改
        return {
            host: config.host,
            uri: config.uri
        };
    }

    /**
     * 更新请求
     * @param request   请求
     * @param config    配置
     * @param dto       传输实体
     */
    updateRequest(request, config, dto) {

        if (config) {
            request.host = config.host;
            request.path = config.uri;
        }

        if (dto) {
            request.headers["content-type"] = "application/json";
            request.body = JSON.stringify(dto);
        }
    }
};