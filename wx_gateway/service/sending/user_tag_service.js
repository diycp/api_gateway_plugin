"use strict";
/**
 * 设置用户tag服务
 * Created by liamjung on 2018/6/28.
 */
const AbstractService = require("../abstract_service");

class UserTagService extends AbstractService {

    /**
     * 执行
     * @param dto       UserTagDTO
     * @param mpConfig  公众号配置
     * @param pipeline  管道
     * @returns {Promise.<void>}
     */
    async execute(dto, mpConfig, pipeline) {

        let wxUserTags = await this.getTags();

        let userTag;

        if (wxUserTags) {
            for (let wxUserTag of wxUserTags.tags) {

                if (dto.tag === wxUserTag.name) {

                    userTag = wxUserTag.id;

                    break;
                }
            }
        }

        if (!userTag) {
            //tag不存在时，创建

            let wxUserTag = await this.createTags(dto.tag);

            userTag = wxUserTag.tag.id;
        }

        let bodyJson = {
            openid_list: [dto.openId],
            tagid: userTag
        };

        super.updateRequest(pipeline.request, undefined, bodyJson);
    }
}

module.exports = new UserTagService();