"use strict";
/**
 * 发送客服消息服务
 * Created by liamjung on 2018/6/28.
 */
const AbstractService = require("../abstract_service");

class KefuMsgService extends AbstractService {

    /**
     * 执行
     * @param dto       KefuMsgDTO
     * @param mpConfig  公众号配置
     * @param pipeline  管道
     * @returns {Promise.<void>}
     */
    async execute(dto, mpConfig, pipeline) {

        let bodyJson = {
            touser: dto.openId,
            msgtype: "text",
            text: {
                content: dto.content
            }
        };

        super.updateRequest(pipeline.request, undefined, bodyJson);
    }
}

module.exports = new KefuMsgService();