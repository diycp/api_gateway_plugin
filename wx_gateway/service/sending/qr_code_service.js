"use strict";
/**
 * 生成二维码ticket服务
 * Created by liamjung on 2018/6/28.
 */
const AbstractService = require("../abstract_service");
const {ResponseError} = require("@libfintech/api-gateway-core/error/response_error");

class QrCodeService extends AbstractService {

    /**
     * 执行
     * @param dto       QRCodeDTO
     * @param mpConfig  公众号配置
     * @param pipeline  管道
     * @returns {Promise.<void>}
     */
    async execute(dto, mpConfig, pipeline) {

        let result;

        try {
            if (dto.expireSeconds) {
                //创建临时二维码
                result = await this.createTmpQRCode(dto.param, dto.expireSeconds);
            } else {
                //创建永久二维码
                result = await this.createLimitQRCode(dto.param);
            }
        } catch (err) {
            //打印日志，忽略
            pipeline.logger.error(JSON.stringify(err));

            result = err;
        }

        let ticket = result.ticket;

        //ResultDTO
        let bodyJson = {
            success: !!ticket,
            errorMsg: !!ticket ? undefined : "二维码创建失败",
            data: !!ticket ? ticket : JSON.stringify(result)
        };

        //终止管道，响应客户端
        throw new ResponseError(200, undefined, JSON.stringify(bodyJson));
    }
}

module.exports = new QrCodeService();