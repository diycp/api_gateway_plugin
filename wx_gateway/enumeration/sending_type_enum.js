"use strict";
/**
 * 发送类型枚举
 * Created by liamjung on 2018/6/28.
 */
const SendingTypeEnum = {
    TEMPLATE: "template",
    QR_CODE: "qr_code",
    MENU_BTN: "menu_btn",
    MSG_TEMP: "msg_temp",
    USER_TAG: "user_tag",
    KEFU_MSG: "kefu_msg"
};

module.exports = SendingTypeEnum;