"use strict";
/**
 * 验证接口
 * 对应微信公众号网页授权域名配置中的一项（公众号设置 -> 功能设置 -> 网页授权域名 -> 设置）
 * Created by liamjung on 2018/6/27.
 */
const {ResponseError} = require("@libfintech/api-gateway-core/error/response_error");
const CODE_PREFIX = "MP_verify_";
const CODE_SUFFIX = ".txt";

module.exports = async function (pipeline) {

    let req = pipeline.request;

    let code = req.originalPath.substring(req.originalPath.lastIndexOf("/") + 1);
    code = code.substring(code.indexOf(CODE_PREFIX) + CODE_PREFIX.length);
    code = code.substring(0, code.indexOf(CODE_SUFFIX));

    //直接响应微信
    throw new ResponseError(200, {}, code);
};