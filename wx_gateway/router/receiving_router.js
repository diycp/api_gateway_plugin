"use strict";
/**
 * 接收路由器
 * Created by liamjung on 2018/6/25.
 */
const menuService = require("../service/receiving/menu_service");
const msgService = require("../service/receiving/msg_service");
const scanService = require("../service/receiving/scan_service");
const subscribeService = require("../service/receiving/subscribe_service");
const unsubscribeService = require("../service/receiving/unsubscribe_service");
const {ResponseWxError} = require("../error/response_wx_error");
const {getMpConfig} = require("@libfintech/api-gateway-core/util/util");

module.exports = async function (wxMsg, pipeline) {

    let req = pipeline.request;

    let mpId = req.originalPath.substring(req.originalPath.lastIndexOf("/") + 1);

    //实时读取配置
    let mpConfig = await getMpConfig(mpId);

    switch (wxMsg.MsgType) {
        //文本消息
        case "text":
            await msgService.execute(wxMsg, mpConfig, pipeline);
            break;
        //事件消息
        case "event":

            switch (wxMsg.Event) {
                //点击菜单（非链接）事件
                case "CLICK":
                    await menuService.execute(wxMsg, mpConfig, pipeline);
                    break;
                //扫码事件
                case "SCAN":
                    await scanService.execute(wxMsg, mpConfig, pipeline);
                    break;
                //关注事件
                case "subscribe":
                    await subscribeService.execute(wxMsg, mpConfig, pipeline);
                    break;
                //取消关注事件
                case "unsubscribe":
                    await unsubscribeService.execute(wxMsg, mpConfig, pipeline);
                    break;
                //其他事件
                default:
                    //其他事件时，直接响应微信
                    pipeline.logger.debug("There is no receiving service (event: " + wxMsg.Event + ")");

                    throw new ResponseWxError("");

                    break;

                    break;
            }

            break;
        //其他消息
        default:
            //其他消息时，直接响应微信
            pipeline.logger.debug("There is no receiving service (msgType: " + wxMsg.MsgType + ")");

            throw new ResponseWxError("");

            break;
    }
};