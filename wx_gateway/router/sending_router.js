"use strict";
/**
 * 发送路由器
 * Created by liamjung on 2018/6/28.
 */
const kefuMsgService = require("../service/sending/kefu_msg_service");
const menuBtnService = require("../service/sending/menu_btn_service");
const msgTempService = require("../service/sending/msg_temp_service");
const qrCodeService = require("../service/sending/qr_code_service");
const templateService = require("../service/sending/template_service");
const userTagService = require("../service/sending/user_tag_service");
const SendingTypeEnum = require("../enumeration/sending_type_enum");
const {ResponseError} = require("@libfintech/api-gateway-core/error/response_error");
const {getMpConfig} = require("@libfintech/api-gateway-core/util/util");
const uriConfigs = {};
//https://github.com/node-webot/co-wechat-api/blob/master/lib/api_template.js
uriConfigs[SendingTypeEnum.TEMPLATE] = "/cgi-bin/message/template/send";
//https://github.com/node-webot/co-wechat-api/blob/master/lib/api_qrcode.js
uriConfigs[SendingTypeEnum.QR_CODE] = "/cgi-bin/qrcode/create";
//https://github.com/node-webot/co-wechat-api/blob/master/lib/api_menu.js
uriConfigs[SendingTypeEnum.MENU_BTN] = "/cgi-bin/menu/create";
//https://github.com/node-webot/co-wechat-api/blob/master/lib/api_message.js
uriConfigs[SendingTypeEnum.MSG_TEMP] = "/cgi-bin/message/custom/send";
//https://github.com/node-webot/co-wechat-api/blob/master/lib/api_user.js
uriConfigs[SendingTypeEnum.USER_TAG] = "/cgi-bin/tags/members/batchtagging";
//https://github.com/node-webot/co-wechat-api/blob/master/lib/api_message.js
uriConfigs[SendingTypeEnum.KEFU_MSG] = "/cgi-bin/message/custom/send";

module.exports = async function (sendingType, dto, pipeline) {

    let uri = uriConfigs[sendingType];

    if (!uri) {
        //配置不存在时，抛出异常

        pipeline.logger.warn("There is no sending type named \"" + sendingType + "\"");

        throw new ResponseError(404);
    }

    this.logger = pipeline.logger;
    const {accessToken} = await this.ensureAccessToken();
    uri += "?access_token=" + accessToken;

    pipeline.request.path = uri;

    //实时读取配置
    let mpConfig = await getMpConfig.call(global, dto.mpId);

    switch (sendingType) {
        //发送模板消息
        case SendingTypeEnum.TEMPLATE:
            await templateService.execute.call(this, dto, mpConfig, pipeline);
            break;
        //生成二维码ticket
        case SendingTypeEnum.QR_CODE:
            await qrCodeService.execute.call(this, dto, mpConfig, pipeline);
            break;
        //创建菜单按钮
        case SendingTypeEnum.MENU_BTN:
            await menuBtnService.execute.call(this, dto, mpConfig, pipeline);
            break;
        //发送普通模板消息
        case SendingTypeEnum.MSG_TEMP:
            await msgTempService.execute.call(this, dto, mpConfig, pipeline);
            break;
        //设置用户tag
        case SendingTypeEnum.USER_TAG:
            await userTagService.execute.call(this, dto, mpConfig, pipeline);
            break;
        //发送客服消息
        case SendingTypeEnum.KEFU_MSG:
            await kefuMsgService.execute.call(this, dto, mpConfig, pipeline);
            break;

        //扩展其他业务
    }
};