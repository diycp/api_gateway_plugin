"use strict";

/**
 * 响应微信异常
 * 用于在出错时，响应微信客户端
 * Created by liamjung on 2018/6/26.
 */

class ResponseWxError extends Error {

    constructor(message) {

        super();

        this._message = message;
    }

    /**
     * 返回消息
     * @returns {*}
     */
    get message() {
        return this._message;
    }
}

module.exports = {
    ResponseWxError
};