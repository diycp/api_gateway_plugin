微信网关插件
======

处理微信请求或发到微信的请求（必须与[微信网关响应](https://gitee.com/libfintech/api_gateway_plugin/blob/master/wx_gateway_response)插件配对使用）

**1. 功能说明**
- 多公众号统一接入&分发微信请求至不同业务服务器
- 提供简单模板消息功能，发送[微信客服消息](https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140547)时，格式化消息
- 提供在线微信公众号调试功能，免去配置内网穿透等困扰（**待实现**）

**2. 插件配置说明**

在[微信API网关](https://gitee.com/libfintech/wechat_api_gateway)的[/plugin_config/wx_gateway](https://gitee.com/libfintech/wechat_api_gateway/blob/master/plugin_config/wx_gateway)和[/plugin_config/wx_gateway_response](https://gitee.com/libfintech/wechat_api_gateway/blob/master/plugin_config/wx_gateway_response)目录中，已经做好插件运行相关配置，无特殊要求，请不要修改。

**3. 公众号配置（支持多公众号）说明**
- 单机模式时，复制[mp_config.json.example](https://gitee.com/libfintech/wechat_api_gateway/blob/master/mp_config.json.example)为mp_config.json（文件名必须与应用配置中的mpConfigName值相同），保存到项目根目录中
- 集群模式时，在redis中以哈希（hash）方式（哈希的redis key必须与应用配置中的mpConfigName值相同）配置公众号，哈希的field为mpId，value为配置参数的json，json格式参考[mp_config.json.example](https://gitee.com/libfintech/wechat_api_gateway/blob/master/mp_config.json.example)

具体配置参数如下：

<table>
 <tr>
     <td>mpId</td>
     <td>自定义公众号唯一标识</td>
 </tr>
 <tr>
     <td>mpName</td>
     <td>自定义公众号名称</td>
 </tr>
 <tr>
     <td>subjectId</td>
     <td>自定义主体id，唯一标识开放平台主体</td>
 </tr>
 <tr>
     <td>type</td>
     <td>类型，mp表示公众号，mini_app表示小程序</td>
 </tr>
 <tr>
     <td>appId</td>
     <td>微信提供，在公众号后台的基本配置中获取</td>
 </tr>
 <tr>
     <td>secret</td>
     <td>微信提供，在公众号后台的基本配置中获取</td>
 </tr>
 <tr>
     <td>token</td>
     <td>微信提供，在公众号后台的基本配置中获取</td>
 </tr>
 <tr>
     <td>aesKey</td>
     <td>微信提供，在公众号后台的基本配置中获取</td>
 </tr>
 <tr>
     <td>templates</td>
     <td>
         <div>微信模板配置：</div>
         <div>业务类型与模板id的映射关系</div>
     </td>
 </tr>
 <tr>
     <td>msgTemps</td>
     <td>
         <div>消息模板配置：</div>
         <div>业务类型与模板路径的映射关系，模板保存到项目根目录中</div>
         <div>消息模板例子：<a href="https://gitee.com/libfintech/wechat_api_gateway/blob/master/template/example.msg">example.msg</a>，其中${name}表示变量语法</div>
     </td>
 </tr>
 <tr>
     <td>routes</td>
     <td>
         <div>路由配置：</div>
         <div>路由类型与业务网址或业务消息映射关系</div>
         <div>路由类型：</div>
         <div>scan：扫码事件，映射到业务网址</div>
         <div>subscribe：订阅事件，映射到业务网址</div>
         <div>menu：菜单点击事件，映射到业务网址</div>
         <div>content：客服消息事件，映射到业务网址或业务消息</div>
     </td>
 </tr>
</table>

**4. 用法说明**

*4.1. 进入微信公众号后台，配置下面三个参数，即可完成统一接入&分发微信请求至不同业务服务器*
- 公众号设置 -> 功能设置 -> 网页授权域名，配置为微信API网关域名
 ![Image text](https://gitee.com/libfintech/wechat_api_gateway/attach_files/download?i=153311&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F04%2F5A%2FPaAvDFtZdR2AQv3qAAECobRzVGE182.png%3Ftoken%3D9a97301b5428e66bae2d84a2c07ded83%26ts%3D1532589353%26attname%3Ddomain_authe.png)
- 安全中心 -> IP白名单，配置为微信API网关域名指向的ip地址
 ![Image text](https://gitee.com/libfintech/wechat_api_gateway/attach_files/download?i=153310&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F04%2F5A%2FPaAvDFtZdRWAFoUGAAEYJho8kB4660.png%3Ftoken%3D29b8b767dcbd93d7407786ad73656bd4%26ts%3D1532589353%26attname%3Dip_white_list.png)
- 基本配置 -> 服务器地址(URL)，配置为http(s)://域名/mp/{mpId}，其中mpId为第三步配置的公众号唯一标识
 ![Image text](https://gitee.com/libfintech/wechat_api_gateway/attach_files/download?i=153312&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F04%2F5A%2FPaAvDFtZdSmABHj5AACCje81VoQ378.png%3Ftoken%3D83bda88a0c95d0bfbd7f7cfad290bf5d%26ts%3D1532589353%26attname%3Dserver_url.png)

*4.2. 发送[微信模板消息](https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1433751277)*

用[TemplateDTO](https://gitee.com/libfintech/wechat_api_gateway_dto/blob/master/src/main/java/com/libfintech/wechat_api_gateway/dto/sending/TemplateDTO.java)，以POST方式调用/send/template REST API

TemplateDTO说明：
- mpId：自定义公众号唯一标识，用于定位公众号
- openId：微信用户openId
- type：模板类型，用于定位模板id，该值对应于公众号配置中templates的业务类型
- url：详情url，用于点击模板消息时，跳转到指定url
- data：数据，用于替换模板中的变量

注：忽略上述几个参数外的所有参数

*4.3. 发送简单模板消息*

用[MsgTempDTO](https://gitee.com/libfintech/wechat_api_gateway_dto/blob/master/src/main/java/com/libfintech/wechat_api_gateway/dto/sending/MsgTempDTO.java)，以POST方式调用/send/msg_temp REST API

MsgTempDTO说明：
- mpId：自定义公众号唯一标识，用于定位公众号
- openId：微信用户openId
- type：消息模板类型，用于定位消息模板id，该值对应于公众号配置中msgTemps的业务类型
- data：数据，用于替换模板中的变量

*4.4. 其他调用微信或接收微信请求相关的dto，请参考[微信API网关DTO](https://gitee.com/libfintech/wechat_api_gateway_dto)项目（Java版本），调用微信的dto参考[sending](https://gitee.com/libfintech/wechat_api_gateway_dto/tree/master/src/main/java/com/libfintech/wechat_api_gateway/dto/sending)目录，接收微信请求的dto参考[receiving](https://gitee.com/libfintech/wechat_api_gateway_dto/tree/master/src/main/java/com/libfintech/wechat_api_gateway/dto/receiving)目录*

*4.5. Java服务调用微信或接收微信请求时，需在项目中引用dto模块*

步骤操作：
- 在[pom.xml](https://gitee.com/libfintech/wechat_api_gateway_dto/blob/master/pom.xml)中，把部署配置的url改成自己的私服地址
- 通过迭代版本号发布版本（mvn deploy）
- 引用dto
```xml
<dependency>
    <groupId>com.libfintech.wechat_api_gateway</groupId>
    <artifactId>dto</artifactId>
    <version>版本号</version>
</dependency>
```


**5. 致谢**

- co-wechat：https://github.com/node-webot/co-wechat
- co-wechat-api：https://github.com/node-webot/co-wechat-api
