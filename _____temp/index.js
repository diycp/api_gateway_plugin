"use strict";
/**
 * ___插件名称___
 * Created by liamjung on 2018/6/22.
 */
const {matchConfig} = require("@libfintech/api-gateway-core/util/util");

//插件入口函数
async function main(pipeline, configs) {

    let config = matchConfig(pipeline.request, configs);

    if (config) {
        //配置存在时的处理

    } else {
        //配置不存在时的处理

    }
}

module.exports = {
    main: main
};